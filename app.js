//memanggil dari folder html
let btnadd = document.getElementById("btnadd");
let btnclear = document.getElementById("btnclear");
let activity = document.getElementById("txtactivity"); //txtactivity hanya memanggil dalam folder html, sedangkan di dalam js hanya menggunakan activity
let activity1 = document.getElementById("txtactivity1");
let list = document.getElementById("listactivity");
let data = JSON.parse(localStorage.getItem('storagetodo'));//
let listvalue = data?data:[]; //untuk mengisi value menggunakan operasi ternary
let el='';
let is_add = true;

//sebelum native, menjalankan program diatas
btnadd.addEventListener('click', function(event){
    let act = activity.value;              
    let act1 = activity1.value;              
    
    if(act !="" && is_add==true)
    {
        listvalue.push(act);//ctt : push menunjukkan isi tanpa menampilkan, sedangkan inner html menampilkan pada browse
        if(act1 !="" && is_add==true)
        {
            listvalue.push(act1);
            if(document.getElementById("lk").checked)
            {
                var l = document.getElementById("lk").value;
                listvalue.push(l);
                if(document.getElementById("islam").selected)
                {
                    var is = document.getElementById("islam").value;
                    listvalue.push(is);
                }
                else if(document.getElementById("kristen").selected)
                {
                    var kr = document.getElementById("kristen").value;
                    listvalue.push(kr);
                }
                else if(document.getElementById("katolik").selected)
                {
                    var ka = document.getElementById("katolik").value;
                    listvalue.push(ka);
                }
                else if(document.getElementById("hindu").selected)
                {
                    var hi = document.getElementById("hindu").value;
                    listvalue.push(hi);
                }
                else if(document.getElementById("buddha").selected)
                {
                    var bu = document.getElementById("buddha").value;
                    listvalue.push(bu);
                }
            }
            if(document.getElementById("pr").checked)
            {
                var p = document.getElementById("pr").value;
                listvalue.push(p);
                if(document.getElementById("islam").selected)
                {
                    var is = document.getElementById("islam").value;
                    listvalue.push(is);
                }
                else if(document.getElementById("kristen").selected)
                {
                    var kr = document.getElementById("kristen").value;
                    listvalue.push(kr);
                }
                else if(document.getElementById("katolik").selected)
                {
                    var ka = document.getElementById("katolik").value;
                    listvalue.push(ka);
                }
                else if(document.getElementById("hindu").selected)
                {
                    var hi = document.getElementById("hindu").value;
                    listvalue.push(hi);
                }
                else if(document.getElementById("buddha").selected)
                {
                    var bu = document.getElementById("buddha").value;
                    listvalue.push(bu);
                }
            }
        }
    }
    else if(act !="" && is_add==false)
    {
        listvalue[index] = act;
        if(act1 !="" && is_add==false)
        {
            listvalue[index] = act1;
            if(document.getElementById("lk").checked)
            {
                var l = document.getElementById("lk").value;
                listvalue.push(l);
                if(document.getElementById("islam").selected)
                {
                    var is = document.getElementById("islam").value;
                    listvalue.push(is);
                }
                else if(document.getElementById("kristen").selected)
                {
                    var kr = document.getElementById("kristen").value;
                    listvalue.push(kr);
                }
                else if(document.getElementById("katolik").selected)
                {
                    var ka = document.getElementById("katolik").value;
                    listvalue.push(ka);
                }
                else if(document.getElementById("hindu").selected)
                {
                    var hi = document.getElementById("hindu").value;
                    listvalue.push(hi);
                }
                else if(document.getElementById("buddha").selected)
                {
                    var bu = document.getElementById("buddha").value;
                    listvalue.push(bu);
                }
            }
            if(document.getElementById("pr").checked)
            {
                var p = document.getElementById("pr").value;
                listvalue.push(p);
                if(document.getElementById("islam").selected)
                {
                    var is = document.getElementById("islam").value;
                    listvalue.push(is);
                }
                else if(document.getElementById("kristen").selected)
                {
                    var kr = document.getElementById("kristen").value;
                    listvalue.push(kr);
                }
                else if(document.getElementById("katolik").selected)
                {
                    var ka = document.getElementById("katolik").value;
                    listvalue.push(ka);
                }
                else if(document.getElementById("hindu").selected)
                {
                    var hi = document.getElementById("hindu").value;
                    listvalue.push(hi);
                }
                else if(document.getElementById("buddha").selected)
                {
                    var bu = document.getElementById("buddha").value;
                    listvalue.push(bu);
                }
            }
        }
    }
    localStorage.setItem('storagetodo',JSON.stringify(listvalue));//untuk melihat local storage pada browser/android
    is_add = true;//untuk mengembalikan nilai awal pada add supaya bisa di add kembali
    display();
})

btnclear.addEventListener('click', function(event){
    //listvalue = [];
    listvalue.splice(0, listvalue.length); //menghapus data
    localStorage.setItem('storagetodo',JSON.stringify(listvalue));//untuk melihat local storage pada browser/android
    display();
})

//harus dijadikan map javascript
function display(){
    el='';
    for(let i=0; i<listvalue.length; i++)
        {
            el +="<li>" + listvalue[i]+"<button onclick='del("+ i +")'> Delete</button> " + 
            "<button onclick='update(" + i + ")'>Update</button></li>";   
        }
        list.innerHTML = el;
        activity.value='';
}

function update(i)
{
    activity.value = listvalue[i];
    is_add = false;
    index = i;
}

//untuk menghapus file yang sudah diinput dari btnadd
function del(index){
    //alert('Hapus' + index);
    listvalue.splice(index, 1); //yang dihapus adalah indexnya/isi valuenya
    localStorage.setItem('storagetodo',JSON.stringify( listvalue));//untuk melihat local storage pada browser/android
    display();
}